DROP DATABASE IF EXISTS Badminton;
CREATE DATABASE Badminton;
USE Badminton;

-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: badminton
-- ------------------------------------------------------
-- Server version	5.7.14-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `ADMIN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ADMIN` (
  `SOEID` varchar(7) NOT NULL,
  `PASSWORD` varchar(15) NOT NULL,
  `ROLE` tinyint(4) NOT NULL,
  PRIMARY KEY (`SOEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `ADMIN` WRITE;
/*!40000 ALTER TABLE `ADMIN` DISABLE KEYS */;
/*!40000 ALTER TABLE `ADMIN` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auction`
--

DROP TABLE IF EXISTS `AUCTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUCTION` (
  `PLAYER_ID` varchar(7) NOT NULL,
  `CAPTAIN_ID` varchar(7) NOT NULL,
  `SOLD_PRICE` tinyint(4) NOT NULL,
  PRIMARY KEY (`PLAYER_ID`),
  KEY `CAPTAIN_ID` (`CAPTAIN_ID`),
  CONSTRAINT `auction_ibfk_1` FOREIGN KEY (`PLAYER_ID`) REFERENCES `PLAYER` (`SOEID`),
  CONSTRAINT `auction_ibfk_2` FOREIGN KEY (`CAPTAIN_ID`) REFERENCES `CAPTAIN` (`SOEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auction`
--

LOCK TABLES `AUCTION` WRITE;
/*!40000 ALTER TABLE `AUCTION` DISABLE KEYS */;
/*!40000 ALTER TABLE `AUCTION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `captain`
--

DROP TABLE IF EXISTS `CAPTAIN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CAPTAIN` (
  `SOEID` varchar(7) NOT NULL,
  `CREDITS` tinyint(4) NOT NULL,
  `STATUS` tinyint(4) NOT NULL DEFAULT '0',
  `PASSWORD` varchar(15) NOT NULL,
  PRIMARY KEY (`SOEID`),
  CONSTRAINT `captain_ibfk_1` FOREIGN KEY (`SOEID`) REFERENCES `PLAYER` (`SOEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `captain`
--

LOCK TABLES `CAPTAIN` WRITE;
/*!40000 ALTER TABLE `CAPTAIN` DISABLE KEYS */;
INSERT INTO `CAPTAIN` VALUES ('AD66452',0,0,'12345'),('AJ66736',0,0,'12345'),('AK67425',0,0,'12345'),('AP66807',0,0,'12345'),('AS67044',0,0,'12345'),('AT66797',0,0,'12345'),('AT67452',0,0,'12345'),('AY67466',0,0,'12345'),('BV67042',0,0,'12345'),('CM67489',0,0,'12345'),('GB67045',0,0,'12345'),('GR68632',0,0,'12345'),('HB66811',0,0,'12345'),('JA66801',0,0,'12345'),('JP67435',0,0,'12345'),('KJ67938',0,0,'12345'),('KS67056',0,0,'12345'),('LT66731',0,0,'12345'),('MS67521',0,0,'12345'),('NK66805',0,0,'12345'),('NR67499',0,0,'12345'),('RL67448',0,0,'12345'),('RP68639',0,0,'12345'),('RR67501',0,0,'12345'),('RS69757',0,0,'12345'),('SB67062',0,0,'12345'),('SJ67493',0,0,'12345'),('SK69079',0,0,'12345'),('SM67523',0,0,'12345'),('SS67877',0,0,'12345'),('TR67540',0,0,'12345'),('VM67429',0,0,'12345'),('VV68045',0,0,'12345');
/*!40000 ALTER TABLE `CAPTAIN` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `PLAYER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PLAYER` (
  `SOEID` varchar(7) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `GENDER` varchar(1) NOT NULL,
  `CONTACT` bigint(20) NOT NULL,
  `WEEK1` tinyint(4) NOT NULL,
  `WEEK2` tinyint(4) NOT NULL,
  `WEEK3` tinyint(4) NOT NULL,
  `WEEK4` tinyint(4) NOT NULL,
  `CAP_PREF` tinyint(4) NOT NULL,
  PRIMARY KEY (`SOEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player`
--

LOCK TABLES `PLAYER` WRITE;
/*!40000 ALTER TABLE `PLAYER` DISABLE KEYS */;
INSERT INTO `PLAYER` VALUES ('AD66452','Aadhithya Dinesh','M',9791152980,1,1,1,0,1),('AJ66736','Ajitaa Jagannathan','F',9445182509,1,1,1,0,1),('AK67425','A Aswin Kumar','M',9380742910,1,1,1,1,1),('AM67432','Amritha Madhavan','F',9790927350,1,0,1,0,0),('AM67469','Anbu Chakravarthy Murugesan','M',9488804145,1,1,1,1,0),('AM67856','M Aishwarya Ponnammal','F',7358508425,1,1,0,0,0),('AP66807','Arjuna Bhanu Prakash','M',9100805086,1,1,1,1,1),('AR67486','Anubbhama Ramakrishnan','F',9486509205,0,1,0,1,0),('AS66458','Abdur Rahman','M',9940532896,0,1,1,0,0),('AS67044','Arvind Sudarsan','M',7708409548,1,1,1,1,1),('AT66797','Aniket Trivedi','M',7587191516,1,1,1,1,1),('AT67452','Adeeb Tahir Rahamathullah','M',7299003427,1,1,1,0,1),('AY67466','Aditya Kumar Yadav','M',9944524710,1,1,1,1,1),('BK66817','Bhavana K','F',9791280457,1,0,0,0,0),('BS66457','Battepati Shanmukha Priya','F',9790482024,1,1,0,0,0),('BV67042','Vasitha Balasubramani','F',7598324711,1,0,1,1,1),('CM67489','Charan M','M',8608374140,1,1,1,1,1),('DS67958','S Deekshith Reddy','M',8897725184,1,1,1,1,0),('GB67045','Gayathri Balaji','F',9444392472,1,0,1,1,1),('GB67513','Gopalakrishnan Balasubramanian','M',9489161525,1,1,1,0,0),('GK67047','Gokul','M',7871796911,1,1,1,0,0),('GK67057','Karunya','F',8939744583,0,0,0,1,0),('GR68632','Gembali Venkata Ratnachandu','M',9940123092,1,1,1,0,1),('HB66811','Harini B','F',8428538359,1,1,1,1,1),('HM67490','Hemalatha Manoharan','F',7502184615,1,1,1,0,0),('JA66801','Jaya','M',9941378145,1,1,1,1,1),('JP67433','Penke John Phinehas','M',9603460037,1,1,1,0,0),('JP67435','Pothineni Jahnavi Swetha','F',9003347551,1,1,1,0,1),('JR66800','Janani','F',9791150172,1,1,0,0,0),('JV66453','Jayasurya Vadivel','M',7708252412,1,1,0,0,0),('KA67517','Karunyadevi','F',7708671697,0,1,1,0,0),('KJ67938','Kartik Jain','M',7999930816,1,1,1,1,1),('KM67446','M Kalyan Venkat','M',9182821521,1,1,0,0,0),('KN67857','Krishnaraj Nedujchezhiyan','M',8523983251,0,1,0,0,0),('KS67056','Karthik','M',9940191782,0,1,1,1,1),('LB68328','Lavina Berlia','F',9748078718,1,1,0,0,0),('LG66803','Lavanya Shanmugam','F',9597890822,1,0,1,0,0),('LS67497','Lakshmi G','F',9597890822,1,0,0,0,0),('LT66731','Lagnesh Sekar','M',9488209987,1,1,0,1,1),('MP67165','P Madhav','M',7200825268,1,1,0,1,0),('ms67498','Manimozhi Sadasivam','F',9003507597,1,1,0,0,0),('MS67521','Mohamed Sarfraz M','M',8056882654,1,1,1,0,1),('NB67433','Nandan Bhaskaran','M',9600116807,1,1,1,1,0),('NC67067','Neeraj Choudhary','M',9840630517,1,1,1,1,0),('NK66805','Nirmal Kumar Kuppusamy','M',9159270048,1,1,1,1,1),('NR67070','Nivethika Rajasekaran','F',9659445188,1,1,1,0,0),('NR67499','Narayana Laskhmi R','F',9626465027,1,1,1,1,1),('NV67527','Nikita Vignesh Kumar','F',9766098056,0,1,0,1,0),('PB67950','Pooja Bhaiya','F',9940085108,1,1,0,0,0),('PG67048','Pallampati Gnana Sai Sri Harsha','M',8333014148,0,1,0,1,0),('PJ67979','Pon Clinton Jose J','M',9894016690,1,1,1,0,0),('PK67731','Prasanth Kalli','M',8333075740,1,1,0,1,0),('PS67050','Pavithra','F',9976611624,0,0,1,1,0),('PV67500','Pradeepa Ann Varghese','F',7358547119,1,0,0,0,0),('RC67858','Roshni Chandrasekar','F',9003135312,0,0,1,1,0),('RJ69069','Ruphus Joel','M',9042313310,1,1,1,1,0),('RL67448','Ramanathan','M',9597656692,1,1,1,1,1),('RM67697','Roshan Safiah Mohamed Rafiq','F',7550058943,1,1,1,1,0),('RP68639','Surya Prakash R','M',8939004749,1,1,0,1,1),('RR67501','Rahul Ranganathan','M',8870550050,1,1,0,1,1),('RR68635','Rohitashva Raj','M',9776120396,1,1,0,0,0),('RS67052','Rangarajan','M',9176784024,1,1,1,0,0),('RS69757','Ramya S','F',9626967140,1,1,0,1,1),('SB67055','Sandhya','F',9159155566,1,1,0,1,0),('SB67062','Shreyas B','M',9500070237,1,1,1,1,1),('SG67863','Shailaesh Gunasekaran','M',9943347046,1,1,0,1,0),('SG67969','Sriram G','M',8438410820,1,1,1,0,0),('SH66813','Sri Hari Sivakumar','M',9629198825,1,1,1,1,0),('SI67538','Surya I','M',8825880872,1,1,1,0,0),('SJ67493','Jitta Sai Chaitanya','F',7358330273,1,1,1,1,1),('SJ67861','Saktheeswaran Jayakumar','M',7358332737,1,1,1,1,0),('SK67453','Srinithi K','F',9789821049,0,1,1,0,0),('SK67510','K Sriram','M',9445815730,1,0,1,1,0),('SK69079','Sanjay Krishna K','M',9944557102,1,1,1,1,1),('SM67523','Madhumitaa Srinivasan','F',9597219991,1,0,1,1,1),('SN69445','Shivanesh Nadar','M',8898350606,1,0,1,1,0),('SP67535','Sakthisri Panchabakesan','F',9003299783,1,1,1,1,0),('SP67863','Premkumar Shruti','M',8870690221,1,1,0,1,0),('SS67877','Sripadam Satish','F',9603659211,1,1,0,1,1),('SV66824','Srinidhi','F',7358452877,1,1,1,0,0),('TR67540','Tamilarasan R','M',8940657620,1,1,1,1,1),('VA69453','Vasudevakrishnan A','M',9884508583,1,1,0,0,0),('VM67429','Venkata Rathnam Muralidharan','M',8148880928,1,1,1,1,1),('VR67457','Ramamoorthy Vivek Narayanan','M',8056266966,1,1,0,1,0),('VR67877','Vignesh R','M',8870573826,1,0,1,1,0),('VS67878','Vigneshwaran S E','M',8056996545,1,1,0,1,0),('VS67882','Vijay Dheepak','M',7358552928,1,1,1,0,0),('VS74619','Vignesh S','M',8903967041,1,1,0,0,0),('VV68045','Vikranth Veeravalli','M',9940112789,1,1,1,1,1),('ZD68321','Zenith Das','M',7579066670,1,1,1,0,0);
/*!40000 ALTER TABLE `PLAYER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preference`
--

DROP TABLE IF EXISTS `PREFERENCE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PREFERENCE` (
  `PLAYER_ID` varchar(7) NOT NULL,
  `CAPTAIN_ID` varchar(7) NOT NULL,
  `RATING` int(11) NOT NULL,
  PRIMARY KEY (`PLAYER_ID`,`CAPTAIN_ID`),
  KEY `CAPTAIN_ID` (`CAPTAIN_ID`),
  CONSTRAINT `preference_ibfk_1` FOREIGN KEY (`PLAYER_ID`) REFERENCES `PLAYER` (`SOEID`),
  CONSTRAINT `preference_ibfk_2` FOREIGN KEY (`CAPTAIN_ID`) REFERENCES `CAPTAIN` (`SOEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preference`
--

LOCK TABLES `PREFERENCE` WRITE;
/*!40000 ALTER TABLE `PREFERENCE` DISABLE KEYS */;
/*!40000 ALTER TABLE `PREFERENCE` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-03  7:39:44

INSERT INTO ADMIN VALUES('AA12345','12345678',0);
INSERT INTO ADMIN VALUES('BB12345','12345678',1);