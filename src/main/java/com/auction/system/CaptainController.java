package com.auction.system;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.auction.system.business.Captain;
import com.auction.system.business.CaptainRepository;
import com.auction.system.business.Preference;
import com.auction.system.business.PreferenceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.logging.LogManager;
@Controller
public class CaptainController {
	private static final Logger LOGGER = LoggerFactory.getLogger(CaptainController.class);	

	@Autowired
	CaptainRepository captainRepository;
	@PostMapping("/captain")
	public String captain(HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) {
		LOGGER.info("Inside CaptainController");
		Captain captain = captainRepository.validateCaptain(request.getParameter("soeid"), 
				request.getParameter("password"));
		if(captain == null || captain.getStatus() != 2) {
			LOGGER.error("Captain Username or Password invalid");
			model.addAttribute("errorMsg","Username/Password Invalid");
			return "error";
		}
		else {
			Cookie cookie = new Cookie("id",request.getParameter("soeid"));
			Cookie passCookie = new Cookie("pass",request.getParameter("password"));
			response.addCookie(cookie);
			response.addCookie(passCookie);
			session.setAttribute("soeid", request.getParameter("soeid"));
			LOGGER.info("Admin with "+request.getParameter("soeid")+" has logged in.");
			List<Preference> prefs=preferenceRepository.getPreferenceByCaptaindId(request.getParameter("soeid"));
			session.setAttribute("status", captain.getStatus());
			if(prefs.isEmpty()) {
				  return "addRatings";
			}
			else {
				return "viewRatings";
			}
		}
	}

	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		LOGGER.info("Captain with "+session.getAttribute("soeid")+" has logged out.");
		session.removeAttribute("soeid");
		session.removeAttribute("status");

		try {
			session.invalidate();
		}
		catch(IllegalStateException e) {
			LOGGER.error("Invalid Session!");
			//e.printStackTrace();
		}
		return "index";
	}

	@RequestMapping("/captainhome")
	public String captainHome(HttpSession session,Model model) {
		
		if (session.getAttribute("soeid") == null || session.getAttribute("status") == null) {
			LOGGER.error("Invalid session or unauthorised Captain!");
			model.addAttribute("errorMsg","You can't access this page");
			return "error";
		} else {
			LOGGER.info("Inside Captain home");
			return "captainhome";
		}
	}
	
	@PostMapping("/makeCaptains")
	public String makeCaptains(HttpServletRequest request,HttpSession session,Model model) {
		//LOGGER.info("Inside Admin Login (maker)");
		if(session.getAttribute("soeid") == null || (int) session.getAttribute("role") != 0) {
			LOGGER.error("Not a priviledged maker!)");
			model.addAttribute("errorMsg", "You don't have privilege for this operation");
			return "error";
		}
		else {
			LOGGER.info("Displaying list of captains for maker");
			String[] captainList = request.getParameterValues("captains");
			List <Captain> captains = new ArrayList<>();
			for (String string : captainList) {
				System.out.println(string);
				captains.add(captainRepository.getCaptainById(string));
			}
			captainRepository.makeCaptains(captains);
			return "finalmaker";
		}
	}

	@PostMapping("/checkCaptains")
	public String checkCaptains(HttpServletRequest request,HttpSession session,Model model) {
		LOGGER.info("Inside Admin Login (checker)");
		if(session.getAttribute("soeid") == null || (int) session.getAttribute("role") != 1) {
			LOGGER.error("Not a priviledged checker!)");
			model.addAttribute("errorMsg", "You don't have privilege for this operation");
			return "error";
		}
		else {
			LOGGER.info("Displaying list of captains for checker");
			String[] soeids = request.getParameterValues("soeid");
			List<Captain> captains = new ArrayList<>();
			for(String string: soeids) {
				System.out.println(string);
				captains.add(captainRepository.getCaptainById(string));
			}
		  captainRepository.checkCaptains(captains);
		  return "finalchecker";
		}
	}
	
	@Autowired
	PreferenceRepository preferenceRepository;

	@PostMapping("/disapprove")
	public String uncheckCaptains(HttpServletRequest request) {
		String[] soeids = request.getParameterValues("soeid");
		List<Captain> captains = new ArrayList<>();
		for(String string: soeids) {
			System.out.println(string);
			captains.add(captainRepository.getCaptainById(string));
		}
		captainRepository.uncheckCaptains(captains);
		return "index";
	}

	@RequestMapping("/addRatings")
	public String addRatings(Model model, HttpSession session) {
		if (session.getAttribute("soeid") == null || session.getAttribute("status") == null) {
			model.addAttribute("errorMsg", "You can't do that");
			return "error";
		}
		else {
			System.out.println("Session id "+ session.getAttribute("soeid"));
			System.out.println("Session status "+ session.getAttribute("status"));
			return "addRatings";
		}
	}

	@RequestMapping("/viewRating")
	public String viewRating() {
		return "viewRatings";
	}

	@RequestMapping("/viewratings")
	public String viewRatings() {
		return "viewRatings";
	}
}
