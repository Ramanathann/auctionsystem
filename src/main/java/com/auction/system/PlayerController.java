package com.auction.system;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import com.auction.system.business.*;
//import com.auction.system.business.PlayerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.logging.LogManager;
@Controller
public class PlayerController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PlayerController.class);
	@Autowired 
	PlayerRepository playerrepo;
	
	@Autowired
	CaptainRepository captainrepo;
	
	@RequestMapping("/")
	String homehandler(HttpSession session) {
/*		if (session.getAttribute("soeid") != null && session.getAttribute("status") != null) {
			return "captainhome";
		} else if(session.getAttribute("soeid") != null && session.getAttribute("role") != null) {
			if((int)session.getAttribute("role") == 0) {
				return "maker";
			}
			if((int)session.getAttribute("role") == 1) {
				
			}
			return "index";
		}*/
		return "index";
	}
	
	@RequestMapping("/players")
	String Playerinsertnewhandler(HttpServletRequest request, Model model) 
	{
	
		//LOGGER.log(Level.FINE, "Testing");
		
		//LOGGER.debug("POST /players request called");
		LOGGER.info("Inside PlayerController");
		if(playerrepo.getPlayerById(request.getParameter("soeid")) == null) {
			String weekValues [] = request.getParameterValues("week") ;
			Boolean[] weekSelected = {false,false,false,false};
			List<Captain> aCaptain = captainrepo.allCaptains();
			for(int i=0;i<weekValues.length;i++) {
				weekSelected[Integer.parseInt(weekValues[i])-1] = true;
			}
			Player aPlayer = new Player(
					request.getParameter("soeid").toUpperCase(),
					request.getParameter("name"),
					request.getParameter("gender"),
					Long.parseLong(request.getParameter("contact")),
					weekSelected[0],
					weekSelected[1],
					weekSelected[2],
					weekSelected[3],
					(Integer.parseInt(request.getParameter("captain"))==1)
			);
			playerrepo.addPlayer(aPlayer);
			if(Integer.parseInt(request.getParameter("captain"))==1) {
				if(aCaptain.isEmpty() || aCaptain.get(0).getStatus() != 2) {
					playerrepo.addCaptain(request.getParameter("soeid"));
				}
			}
			//model.addAttribute("Players",playerrepo.allPlayers());
			LOGGER.info("User Registered with SOEID "+request.getParameter("soeid"));
			return "index";
		}
		else {
			LOGGER.error("Duplicate SOEID is entered");
			return "DuplicatePKError";
		}
		
	}

	@RequestMapping("/test")
	String testhandler(HttpServletRequest request, Model model) {
		return "bidding";
	}
	
	@RequestMapping("/players/{pageid}")
	String paginatePlayers(@PathVariable int pageid, Model mode) {
		return "";
	}
}