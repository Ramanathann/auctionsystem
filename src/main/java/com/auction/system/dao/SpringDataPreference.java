package com.auction.system.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.auction.system.business.Captain;
import com.auction.system.business.Player;
import com.auction.system.business.Preference;
import com.auction.system.business.PreferenceData;

@Repository
public class SpringDataPreference implements PreferenceData {

	@Autowired
	private JdbcTemplate template;
	
	@Override
	public List<Preference> getSortedPlayersByPref() {
		// TODO Auto-generated method stub
		String sql = "SELECT player_id, captain_id, AVG(Rating) Rating FROM Preference GROUP BY player_id ORDER BY Rating DESC".toUpperCase();
		try {
			return template.query(sql, new PreferenceRowMapper());
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Player> getRoundAPlayers() {
		// TODO Auto-generated method stub
		int limit = 30;
		String sql = "SELECT soeid, name, gender, contact, week1, week2, week3, week4, cap_pref FROM Player P JOIN (SELECT player_id, captain_id, AVG(Rating) Rating FROM Preference GROUP BY player_id) as T ON P.soeid=T.player_id ORDER BY T.Rating DESC LIMIT ".toUpperCase() + limit;
		return template.query(sql, new PlayerRowMapper());
	}

	@Override
	public List<Preference> getPreference() {
		List<Preference> prefs;
		// TODO Auto-generated method stub
		String sql = "SELECT preference.player_id,captain_id,rating,player.name  FROM preference JOIN Player ON Player.soeid=Preference.player_id order by Preference.captain_id,rating desc ".toUpperCase();
		try {
			prefs = template.query(sql, new PreferenceWithNameRowMapper());
			return prefs;
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public List<Preference> getPreferenceByCaptainId(String captainId) {
		List<Preference> prefs;
		// TODO Auto-generated method stub
		String sql = "SELECT preference.player_id,captain_id,rating,player.name  FROM preference JOIN Player ON Player.soeid=Preference.player_id WHERE captain_id = ? order by rating desc".toUpperCase();
		try {
			prefs = template.query(sql, new PreferenceWithNameRowMapper(),captainId);
			return prefs;
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

}
class PreferenceRowMapper implements RowMapper<Preference>{

	@Override
	public Preference mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return new Preference(rs.getString("player_id".toUpperCase()), rs.getString("captain_id".toUpperCase()), rs.getDouble("rating".toUpperCase()),null);
	}
}
class PreferenceWithNameRowMapper implements RowMapper<Preference>{

	@Override
	public Preference mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return new Preference(rs.getString("player_id".toUpperCase()),
				rs.getString("captain_id".toUpperCase()), 
				rs.getDouble("rating".toUpperCase()),
				rs.getString("name".toUpperCase()));
	}
}