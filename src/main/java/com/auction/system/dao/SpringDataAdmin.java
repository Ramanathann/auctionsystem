package com.auction.system.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.auction.system.business.Admin;
import com.auction.system.business.AdminData;

@Repository
public class SpringDataAdmin implements AdminData {

	@Autowired
	private JdbcTemplate template;

	@Override
	public List<Admin> getAllAdmins() {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM Admin".toUpperCase();
		
		try {
			return template.query(sql, new AdminRowMapper());
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	@Override
	public Admin getAdminById(String soeid) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM Admin WHERE SOEID=?".toUpperCase();
		try {
			return template.queryForObject(sql, new AdminRowMapper(),soeid);
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public Admin getAdminByIdAndPassword(String soeid, String password) {
		String sql = "SELECT * FROM Admin WHERE SOEID=? AND PASSWORD=?".toUpperCase();
		try {
			return template.queryForObject(sql, new AdminRowMapper(),soeid,password);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
}

class AdminRowMapper implements RowMapper<Admin> {

	@Override
	public Admin mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return new Admin(rs.getString("SOEID"), rs.getString("PASSWORD"), rs.getInt("ROLE"));
	}
}