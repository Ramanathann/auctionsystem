package com.auction.system.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.auction.system.business.Captain;
import com.auction.system.business.CaptainData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.logging.LogManager;
@Repository
public class SpringDataCaptains implements CaptainData {
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringDataCaptains.class);	
	
	@Autowired
	JdbcTemplate template;

	@Override
	public List<Captain> allCaptains() {
		// TODO Auto-generated method stub
		String sql = "SELECT captain.soeid, credits, status, password, player.name name FROM Captain JOIN Player ON Captain.soeid=Player.soeid".toUpperCase();
		try {
			return template.query(sql, new CaptainWithNameRowMapper());
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	@Override
	public Captain getCaptainById(String id) {
		// TODO Auto-generated method stub
		LOGGER.info("Getting captain by ID "+id);
		String sql = "SELECT * FROM Captain WHERE SOEID = ?".toUpperCase();
		try {
			Captain captain = template.queryForObject(sql,new CaptainRowMapper(),id);
			return captain;
		}
		catch(EmptyResultDataAccessException e) {
			LOGGER.error(" Null exception:SOEID not found in Captain table");
			return null;
		}
	}

	@Override
	public Captain validateCaptain(String id, String password) {
		// TODO Auto-generated method stub
		LOGGER.info("Validating Captain");
		String sql = "SELECT * FROM Captain WHERE SOEID = ? AND PASSWORD = ?".toUpperCase();
		try {
			Captain captain = template.queryForObject(sql,new CaptainRowMapper(),id,password);
			return captain;
		}
		catch(EmptyResultDataAccessException e) {
			LOGGER.error("Invalid password for Captain!");
			return null;
		}
	}

	@Override
	public int makeCaptains(List<Captain> captains) {
		// TODO Auto-generated method stub
		String sql;
		LOGGER.warn("Selecting Captains(maker)");
		for (Captain captain : captains) {
			sql = "UPDATE Captain SET status = ? WHERE soeid = ?".toUpperCase();
			template.update(sql, 1,captain.getId());
		}
		if(captains.size()<6)
		{
			LOGGER.error("Six captains need to be selected!");
		}
		return 1;
	}

	@Override
	public List<Captain> getNominatedCaptains() {
		// TODO Auto-generated method stub
		LOGGER.info("Nominating Captains (checker)");
		List<Captain> captains;
		String sql = "SELECT captain.soeid, credits, status, password, player.name name FROM Captain JOIN Player ON Captain.soeid=Player.soeid WHERE Status=?".toUpperCase();
		try {
			captains = template.query(sql, new CaptainWithNameRowMapper(), 1);
			return captains;
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			LOGGER.error("Captain details not found!");
			return null;
		}
	}

	@Override
	public int checkCaptains(List<Captain> captains) {
		// TODO Auto-generated method stub
		LOGGER.info("Approving Captains");
		String sql;
		for (Captain captain : captains) {
			sql = "UPDATE Captain SET status = ? WHERE soeid = ?".toUpperCase();
			template.update(sql, 2,captain.getId());
		}
		sql = "DELETE FROM Captain WHERE Status!=2".toUpperCase();
		try {
			template.update(sql);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
		}
		return 1;
	}

	@Override
	public int uncheckCaptains(List<Captain> captains) {
		// TODO Auto-generated method stub
		String sql;
		for (Captain captain : captains) {
			sql = "UPDATE Captain SET status = ? WHERE soeid = ?".toUpperCase();
			template.update(sql, 0,captain.getId());
		}
		return 0;
	}

    public int putRatings(String p_id, String c_id, int rating) {
		// TODO Auto-generated method stub
		  String sql = "INSERT INTO preference VALUES(?,?,?)".toUpperCase();
		  template.update(sql,p_id,c_id,rating);
		  return 1;
	  }
}

class CaptainRowMapper implements RowMapper<Captain>{

	@Override
	public Captain mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return new Captain(rs.getString("SOEID"),
				rs.getInt("credits".toUpperCase()),
				rs.getInt("status".toUpperCase()),
				rs.getString("password".toUpperCase()), null);
	}
}

class CaptainWithNameRowMapper implements RowMapper<Captain>{

	@Override
	public Captain mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return new Captain(rs.getString("SOEID"),
				rs.getInt("credits".toUpperCase()),
				rs.getInt("status".toUpperCase()),
				rs.getString("password".toUpperCase()),
				rs.getString("name".toUpperCase()));
	}
}