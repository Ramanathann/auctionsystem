package com.auction.system.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.auction.system.business.Auction;
import com.auction.system.business.AuctionData;
import com.auction.system.business.Captain;
import com.auction.system.business.Player;
import com.auction.system.business.TeamStatus;

@Repository
public class SpringDataAuction implements AuctionData {

	@Autowired
	private JdbcTemplate template;

	@Override
	public List<Auction> getPlayersByCaptain(String soeid) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM Auction WHERE captain_id=?".toUpperCase();
		return template.query(sql, new AuctionRowMapper(), soeid);
	}

	@Override
	public int getSoldPriceById(String soeid) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM Auction WHERE player_id=?".toUpperCase();
		return template.queryForObject(sql, new AuctionRowMapper(), soeid).getSold_price();
	}

	@Override
	public int getNoOfPlayersByCaptain(String soeid) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM Auction WHERE captain_id=?".toUpperCase();
		return template.query(sql, new AuctionRowMapper(), soeid).size();
	}

	@Override
	public Map<String, List<Player> > getPlayersAndCaptain(List<Captain> captains) {
		// TODO Auto-generated method stub
		Map<String, List<Player> > playersByCaptainMap = new HashMap<>();
		for (Captain captain : captains) {
			List<Player> playersList = new ArrayList<>();
			String sql = "SELECT * FROM Player WHERE SOEID=?".toUpperCase();
			playersList.add(template.queryForObject(sql, new PlayerRowMapper(), captain.getId()));
			sql = "SELECT SOEID, NAME, GENDER, CONTACT, WEEK1, WEEK2, WEEK3, WEEK4, CAP_PREF FROM Auction a JOIN Player p on a.PLAYER_ID=p.SOEID WHERE captain_id=?".toUpperCase();
			playersList.addAll(template.query(sql, new PlayerRowMapper(), captain.getId()));
			playersByCaptainMap.put(captain.getId(), playersList);
		}
		return playersByCaptainMap;
	}

	@Override
	public Map<String, Integer> getNoOfPlayersByAllCaptains(List<Captain> captains) {
		// TODO Auto-generated method stub
		Map<String, Integer> noOfPlayersByCaptainMap = new HashMap<>();
		for (Captain captain: captains) {
			noOfPlayersByCaptainMap.put(captain.getId(), getPlayersByCaptain(captain.getId()).size());
		}
		return noOfPlayersByCaptainMap;
	}
	
	@Override
	public int addAuctionRecord(Auction auction) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO Auction VALUES(?,?,?)".toUpperCase();
		return template.update(sql, auction.getPlayer_id(), auction.getCaptain_id(), auction.getSold_price());
	}

	@Override
	public List<TeamStatus> getAllTeamStatus() {
		// TODO Auto-generated method stub
		String sql = "SELECT captain_id, COUNT(*) no_of_players, SUM(sold_price) moneySpent FROM Auction a JOIN Captain c ON a.captain_id = c.soeid GROUP BY a.captain_id".toUpperCase();
		return template.query(sql, new TeamStatusRowMapper());
	}
	
	@Override
	public List<Player> getRoundBPlayers() {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM player AS p WHERE soeid NOT IN (SELECT player_id id FROM Auction) AND soeid NOT IN (SELECT soeid id FROM Captain)".toUpperCase();
		return template.query(sql, new PlayerRowMapper());
	}
}

class AuctionRowMapper implements RowMapper<Auction> {

	@Override
	public Auction mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return new Auction(rs.getString("PLAYER_ID"), rs.getString("CAPTAIN_ID"), rs.getInt("SOLD_PRICE"));
	}
}

class TeamStatusRowMapper implements RowMapper<TeamStatus>{
	
	@Override
	public TeamStatus mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new TeamStatus(rs.getString("captain_id".toUpperCase()), rs.getInt("no_of_players".toUpperCase()), rs.getInt("moneySpent".toUpperCase()));
	}
}