package com.auction.system.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.auction.system.business.*;

@Repository
public class SpringDataPlayers implements PlayerData {
	@Autowired
	JdbcTemplate template;
	
	@Override
	public List<Player> allPlayers() {
		String sql = "SELECT * FROM PLAYER";
		try {
			return template.query(sql, new PlayerRowMapper());
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	@Override
	public Player getPlayerById(String id) {
		String sql = "SELECT * FROM PLAYER WHERE SOEID = ?";
		try {
			Player player = template.queryForObject(sql,new PlayerRowMapper(),id);
			return player;
		}
		catch(EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public int deletePlayerById(String id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Player addPlayer(Player player) {
		String sql = "INSERT INTO Player(soeid,name,gender,contact,"
				+ "week1,week2,week3,week4,cap_pref) "
				+ "VALUES(?,?,?,?,?,?,?,?,?)";
		sql = sql.toUpperCase();
		
		template.update(sql,player.getId(),player.getName(),player.getGender(),
			player.getPhone(),player.isWeek1(),player.isWeek2(),
			player.isWeek3(),player.isWeek4(),player.isCap_pref());
		return player;
	}
	
	@Override
	public int addCaptain(String id) {
		String sql = "INSERT INTO Captain(soeid,credits,status,password)"
				+ "VALUES(?,0,0,'12345')";
		sql = sql.toUpperCase();

		template.update(sql,id);
		return 1;
	}

	@Override
	public int addPreference(int value) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Player> getCaptains() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Player> playersByPage(int pageid) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM Player LIMIT ?,?".toUpperCase();
		return template.query(sql, new PlayerRowMapper(), pageid, 10);
	}

	@Override
	public List<Player> onlyPlayers() {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM PLAYER WHERE SOEID NOT IN (SELECT soeid FROM captain)".toUpperCase();
		try {
			 return template.query(sql,new PlayerRowMapper());
		
		}
		catch(DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}
}

class PlayerRowMapper implements RowMapper<Player>{

	@Override
	public Player mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return new Player(rs.getString("SOEID"),
					rs.getString("name".toUpperCase()),
					rs.getString("gender".toUpperCase()),
					rs.getLong("contact".toUpperCase()),
					Integer.parseInt(rs.getString("week1".toUpperCase()))==1,
					Integer.parseInt(rs.getString("week2".toUpperCase()))==1,
					Integer.parseInt(rs.getString("week3".toUpperCase()))==1,
					Integer.parseInt(rs.getString("week4".toUpperCase()))==1,
					Integer.parseInt(rs.getString("cap_pref".toUpperCase()))==1);
	}
}