package com.auction.system;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.auction.system.business.Captain;
import com.auction.system.business.CaptainRepository;
import com.auction.system.business.Preference;
import com.auction.system.business.PreferenceRepository;
import com.auction.system.business.Ratings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.logging.LogManager;
@RestController
@CrossOrigin
public class CaptainRestController {
	private static final Logger LOGGER = LoggerFactory.getLogger(CaptainRestController.class);
	@Autowired
	private CaptainRepository captainRepo;
	@Autowired
	private PreferenceRepository preferenceRepo;

	@RequestMapping(method = RequestMethod.GET, value="/allCaptains")
	public List<Captain> allCaptains(){
		//LOGGER.info("All captains listed");
		return captainRepo.allCaptains();
	}
	

	@RequestMapping(method = RequestMethod.GET, value="/getNomCaptains")
	public List<Captain> getNominatedCaptains(){
		return captainRepo.getNominatedCaptains();
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/viewRatings")
	public List<Preference> getPreferenceByCaptainId(HttpSession session){
		String captain_id = (String) session.getAttribute("soeid");
		return preferenceRepo.getPreferenceByCaptaindId(captain_id);
	}

	@RequestMapping(method = RequestMethod.POST, value="/putratings")
	public @ResponseBody int updateRatings(@RequestBody List<Ratings> ratings,
			HttpServletRequest request, HttpSession session) {
//		String c_id = null;
//		Cookie[] ck = request.getCookies();
//		if(ck != null) {
//			for(Cookie c:ck) {
//				if(c.getName().equalsIgnoreCase("id")) {
//					c_id = c.getValue();
//				}
//			}
//		}
		for(Ratings rating: ratings) {
			String p_id = rating.getPlayer_id();
			String c_id = (String) session.getAttribute("soeid");
			int rate = rating.getRatings();
			captainRepo.putRatings(p_id, c_id, rate);
		}
		return 1;
	}
}