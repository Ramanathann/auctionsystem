package com.auction.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.auction.system.business.CaptainRepository;
import com.auction.system.business.Player;
import com.auction.system.business.PlayerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.logging.LogManager;
@RestController
@CrossOrigin
public class PlayerRestController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PlayerRestController.class);
	@Autowired
	PlayerRepository playerrepo;

	@Autowired
	CaptainRepository captainrepo;
	
	@RequestMapping(method=RequestMethod.GET, value="/allplayers")
	public List<Player> allPlayers() {
		//LOGGER.info("GET /allplayers request called");
		return playerrepo.allPlayers();
	}

	@RequestMapping(method=RequestMethod.GET, value="/onlyPlayers")
	public List<Player> onlyPlayers(){
		//LOGGER.info("GET /onlyPlayers request called");
		return playerrepo.onlyPlayers();
	}

	@RequestMapping(method=RequestMethod.GET, value="/players/{pageid}")
	public List<Player> playersByPage(int pageid){
		//LOGGER.info("GET /players/pageid request called");
		return playerrepo.playersByPage(pageid);
	}
}