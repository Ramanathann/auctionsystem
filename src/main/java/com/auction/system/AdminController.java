package com.auction.system;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.auction.system.business.Admin;
import com.auction.system.business.AdminRepository;
import com.auction.system.business.Captain;
import com.auction.system.business.CaptainRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.logging.LogManager;

@Controller
public class AdminController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);
	@Autowired
	private AdminRepository adminrepo;
	@Autowired
	private CaptainRepository captainrepo;

	@PostMapping(value = "/adminlog")
	public String validateAdmins(HttpServletRequest request, Model model) {
		LOGGER.info("Inside AdminController");
		Admin admin = adminrepo.getAdminById(request.getParameter("soeid"));
		if(admin != null){
			if (admin.getPassword().equals(request.getParameter("password"))) {
				request.getSession().setAttribute("soeid", admin.getSoeid());
				request.getSession().setAttribute("role", admin.getRole());
				System.out.println("Login Success");
				List<Captain> acaptain =captainrepo.allCaptains();
				List<Captain> captain =captainrepo.getNominatedCaptains();
				
				
				if (admin.getRole()==0 && !(captain.isEmpty())) {
					return "finalmaker";
				}
				if( acaptain.get(0).getStatus()==2) {
					return "finalchecker";
				}
				else if(admin.getRole()==0 && captain.isEmpty()) {
					return "maker";
				}
				else if (admin.getRole()==1 &&  acaptain.get(0).getStatus()==2){
					return "finalchecker";
				}
				else if(admin.getRole()==1) {
					return "checker";
				}
				else if(admin.getRole()==1 && !(captain.isEmpty())) {
					return "invalidChecker";
				}
				
			}
			else {
				model.addAttribute("errorMsg", "Username or Password Invalid");
			    LOGGER.error("Invalid Admin Details!");
				return "error";
			}
		}
		model.addAttribute("errorMsg", "Username or Password Invalid");
	    LOGGER.error("Invalid Admin Details!");
		return "error";
	}

	@RequestMapping("/maker")
	public String maker(Model model, HttpSession session) {
		//LOGGER.info("Inside Admin Login (maker)");
		if(session.getAttribute("soeid") == null || (int) session.getAttribute("role") != 0) {
			LOGGER.error("Not a priviledged maker");
			model.addAttribute("errorMsg", "You don't have privilege for this operation");
			return "error";
		}
		else {
			LOGGER.info("Inside Admin Login (maker)");
			return "maker";			

		}
	}

	@RequestMapping("/checker")
	public String checker(Model model, HttpSession session) {
		if(session.getAttribute("soeid") == null || (int) session.getAttribute("role") != 1) {
			LOGGER.error("Not a priviledged checker");
			model.addAttribute("errorMsg", "You don't have privilege for this operation");
			return "error";
		}
		else {
			LOGGER.info("Inside Admin Login (checker)");
			return "checker";
		}
	}

	@RequestMapping("/adminhome")
	public String adminHome(Model model, HttpSession session) {
		if (session.getAttribute("soeid") == null || session.getAttribute("role") == null) {
			model.addAttribute("errorMsg", "You are not allowed to access this");
			return "error";
		}
		else {
			LOGGER.info("Inside Admin Home");
			return "adminhome";
		}
	}

	@RequestMapping("/auction")
	public String auction(Model model, HttpSession session) {
		if (session.getAttribute("soeid") == null || session.getAttribute("role") == null) {
			model.addAttribute("errorMsg", "You are not allowed to access this");
			return "error";
		}
		else {
			return "bidding";
		}
	}
	@RequestMapping("/adminlogout")
	public String adminlogout(HttpSession session) {
		LOGGER.info("Admin with "+session.getAttribute("soeid")+" has logged out.");
		session.removeAttribute("soeid");
		session.removeAttribute("role");
		try {
			session.invalidate();
		}
		catch(IllegalStateException e) {
			LOGGER.error("Invalid Session");
//			e.printStackTrace();
		}
		return "index";
	}
}