package com.auction.system;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.auction.system.business.Admin;
import com.auction.system.business.AdminRepository;
import com.auction.system.business.Auction;
import com.auction.system.business.AuctionRepository;
import com.auction.system.business.Captain;
import com.auction.system.business.CaptainRepository;
import com.auction.system.business.Player;
import com.auction.system.business.PlayerRepository;
import com.auction.system.business.PreferenceRepository;
import com.auction.system.business.TeamStatus;

@RestController
@CrossOrigin
public class AdminRestController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminRestController.class);

	@Autowired
	private AdminRepository adminrepo;

	@Autowired
	private AuctionRepository auctionrepo;

	@Autowired
	private CaptainRepository captainrepo;

	@Autowired
	private PreferenceRepository prefrepo;
	
	@Autowired
	private PlayerRepository playerrepo;

	private List<Player> playersRoundA = new LinkedList<>();
	private List<Player> playersRoundB = new LinkedList<>();
	private boolean roundA = true;

	@RequestMapping(value = "/adminlogin")
	public boolean validateAdmin(HttpServletRequest request) {
		LOGGER.info("Inside AdminRestController");
		Admin admin = adminrepo.getAdminByIdAndPassword(request.getParameter("soeidAdmin"),
				request.getParameter("password"));
		if (admin != null) {
			request.getSession().setAttribute("soeid", admin.getSoeid());
			request.getSession().setAttribute("role", admin.getRole());
			return true;
		}
		LOGGER.error("Invalid admin details");
		return false;
	}

	@RequestMapping(value = "/viewteams")
	public Map<String, List<Player>> getTeams(HttpServletRequest request) {
		List<Captain> captains = captainrepo.allCaptains();
		return auctionrepo.getPlayersAndCaptain(captains);
	}

	@RequestMapping(value = "/updateteaminfo")
	public List<TeamStatus> getPlayersCount(HttpServletRequest request) {
		return auctionrepo.getAllTeamStatus();
	}

	@RequestMapping(value = "/nextbid")
	public Player getNextPlayer(HttpServletRequest request) {
		if (playersRoundA.isEmpty()) {
			LOGGER.warn("Players for Round A is empty!");
			if (!roundA) {
				return null;
			}
			playersRoundA = prefrepo.getRoundAPlayers();
			if (!playersRoundA.isEmpty()) {
				roundA = false;
			}
		}
		return playersRoundA.isEmpty() ? null : playersRoundA.get(0);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/addbid")
	public boolean insertBid(@RequestBody Auction bidInfo) {
		String captainid = bidInfo.getCaptain_id();
		int price = Integer.valueOf(bidInfo.getSold_price());
		if (price != 0 && captainid.length() > 0) {
			// Player player = playersRoundA.get(0);
			// Auction newAuctionRec = new Auction(player.getId(), captainid, price);
			// auctionrepo.addAuctionRecord(newAuctionRec);
			auctionrepo.addAuctionRecord(bidInfo);
			playersRoundA.remove(0);
		} else {
			playersRoundA.add(playersRoundA.remove(0));
		}
		return true;
	}

	@RequestMapping(value = "/roundbplayers")
	public List<Player> getRoundBPlayers(HttpServletRequest request){
		playersRoundB = auctionrepo.getRoundBPlayers();
		return playersRoundB;
	}
	
	@RequestMapping(value = "/endbid")
	public boolean executeNextRound(HttpServletRequest request) {
		List<Player> allPlayers = playerrepo.allPlayers();
 		List<Captain> captains = captainrepo.allCaptains();
		List<Integer> noOfPlayers = new ArrayList<>();
		int teamSize = allPlayers.size() / captains.size();
		int remainder = allPlayers.size() % captains.size();
		playersRoundA.clear();
		for (Captain captain : captains) {
			noOfPlayers.add(auctionrepo.getNoOfPlayersByCaptain(captain.getId()));
		}

		int price = 0;
		Random random = new Random();
		try {
			while (playersRoundB.size() > 0 && captains.size() > 0) {
				int cap_choice = random.nextInt((captains.size()));
				int player_choice = random.nextInt((playersRoundB.size()));
				Auction auction = new Auction(playersRoundB.get(player_choice).getId(),
						captains.get(cap_choice).getId(), price);
				auctionrepo.addAuctionRecord(auction);
				playersRoundB.remove(player_choice);
				noOfPlayers.set(cap_choice, noOfPlayers.get(cap_choice) + 1);
				if (noOfPlayers.get(cap_choice) == (teamSize - 1) && captains.size() > remainder) {
					captains.remove(cap_choice);
					noOfPlayers.remove(cap_choice);
				}
				if (captains.size() == remainder) {
					teamSize += 1;
					remainder = 0;
				}
			}
			return true;
		} catch (Exception exception) {
			LOGGER.error("Players for Round B is empty!");
			return false;
		}
	}

	@RequestMapping(value = "/logoutadmin")
	public boolean logoutAdmin(HttpServletRequest request) {
		LOGGER.info("Admin with " + request.getSession().getAttribute("soeid") + " has logged out.");
		try {
			request.getSession().removeAttribute("soeid");
			request.getSession().removeAttribute("role");
			return true;
		} catch (Exception exception) {
			LOGGER.error("Invalid session!");
			return false;
		}
	}
}