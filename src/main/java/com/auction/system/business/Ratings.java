package com.auction.system.business;

public class Ratings {
	private String player_id;
	private int ratings;
	public Ratings(String player_id, int ratings) {
		super();
		this.player_id = player_id;
		this.ratings = ratings;
	}
	public String getPlayer_id() {
		return player_id;
	}
	public void setPlayer_id(String player_id) {
		this.player_id = player_id;
	}
	public int getRatings() {
		return ratings;
	}
	public void setRatings(int ratings) {
		this.ratings = ratings;
	}
}
