package com.auction.system.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PlayerRepository {
		private PlayerData dao;

		@Autowired
		public PlayerRepository(PlayerData Player) {
			this.dao = Player;
		}
		
		public List<Player> allPlayers(){
			return dao.allPlayers();
		}
		
		public Player getPlayerById(String id) {
			return dao.getPlayerById(id);
		}
		
		public int deletePlayerById(String id) {
			return dao.deletePlayerById(id);
		}
		
		public Player addPlayer(Player player) {
			return dao.addPlayer(player);
		}
		
		public int addCaptain(String id) {
			return dao.addCaptain(id);
		}
		
		public int addPreference(int id) {
			return dao.addPreference(id);
		}

		public List<Player> playersByPage(int pageid){
			return dao.playersByPage(pageid);
		}
		
		public List<Player> onlyPlayers(){
			return dao.onlyPlayers();
		}
	}
