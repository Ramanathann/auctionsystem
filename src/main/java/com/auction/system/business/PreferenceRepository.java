package com.auction.system.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PreferenceRepository {
	
	private PreferenceData dao;
	
	@Autowired
	public PreferenceRepository(PreferenceData pref) {
		this.dao = pref;
	}
	
	public List<Preference> getSortedPlayersByPref(){
		return this.dao.getSortedPlayersByPref();
	}
	
	public List<Player> getRoundAPlayers(){
		return this.dao.getRoundAPlayers();
	}
	public List<Preference> getPreference(){
		return this.dao.getPreference();
	}
	public List<Preference> getPreferenceByCaptaindId(String captainId){
		return this.dao.getPreferenceByCaptainId(captainId);
	}
}