package com.auction.system.business;

public class Preference {
	private String player_id;
	private String captain_id;
	private double rating;
	private String name;
	
	public Preference(String player_id, String captain_id, double rating, String name) {
		super();
		this.player_id = player_id;
		this.captain_id = captain_id;
		this.rating = rating;
		this.setName(name);
	}
	
	public String getPlayer_id() {
		return player_id;
	}
	public void setPlayer_id(String player_id) {
		this.player_id = player_id;
	}
	public String getCaptain_id() {
		return captain_id;
	}
	public void setCaptain_id(String captain_id) {
		this.captain_id = captain_id;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
