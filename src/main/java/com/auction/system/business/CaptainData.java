package com.auction.system.business;

import java.util.List;

public interface CaptainData {
	public List<Captain> allCaptains();
	public Captain getCaptainById(String id);
	public Captain validateCaptain(String id, String password);
	public int makeCaptains(List<Captain> captains);
	public List<Captain> getNominatedCaptains();
	public int checkCaptains(List<Captain> captains);
	public int uncheckCaptains(List<Captain> captains);
	public int putRatings(String p_id, String c_id,int rating);
}
