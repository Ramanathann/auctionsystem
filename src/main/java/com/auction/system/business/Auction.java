package com.auction.system.business;

public class Auction {
	private String player_id;
	private String captain_id;
	private int sold_price;

	public Auction(String player_id, String captain_id, int sold_price) {
		super();
		this.player_id = player_id;
		this.captain_id = captain_id;
		this.sold_price = sold_price;
	}

	public String getPlayer_id() {
		return player_id;
	}

	public void setPlayer_id(String player_id) {
		this.player_id = player_id;
	}

	public String getCaptain_id() {
		return captain_id;
	}

	public void setCaptain_id(String captain_id) {
		this.captain_id = captain_id;
	}

	public int getSold_price() {
		return sold_price;
	}

	public void setSold_price(int sold_price) {
		this.sold_price = sold_price;
	}

}
