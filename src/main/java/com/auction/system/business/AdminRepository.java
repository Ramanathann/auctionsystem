package com.auction.system.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminRepository {
	private AdminData dao;
	
	@Autowired
	public AdminRepository(AdminData adminData) {
		this.dao = adminData;
	}
	
	public Admin getAdminById(String soeid) {
		return this.dao.getAdminById(soeid);
	}
	
	public Admin getAdminByIdAndPassword(String soeid, String password) {
		return this.dao.getAdminByIdAndPassword(soeid, password);
	}
	
	public List<Admin> getAllAdmins(){
		return this.dao.getAllAdmins();
	}
}
