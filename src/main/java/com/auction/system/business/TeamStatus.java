package com.auction.system.business;

public class TeamStatus {
	private String captainId;
	private int playersBought;
	private int moneySpent;
	
	public TeamStatus(String captainId, int playersBought, int moneySpent) {
		super();
		this.captainId = captainId;
		this.playersBought = playersBought;
		this.moneySpent = moneySpent;
	}
	
	public String getCaptainId() {
		return captainId;
	}
	public void setCaptain_id(String captainId) {
		this.captainId = captainId;
	}
	public int getPlayersBought() {
		return playersBought;
	}
	public void setPlayersBought(int playersBought) {
		this.playersBought = playersBought;
	}
	public int getMoneySpent() {
		return moneySpent;
	}
	public void setMoneySpent(int moneySpent) {
		this.moneySpent = moneySpent;
	}
}
