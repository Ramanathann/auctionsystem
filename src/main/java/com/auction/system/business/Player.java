package com.auction.system.business;

public class Player {
	private String id;
	private String name;
	private String gender;
	private long phone;
	private boolean week1;
	private boolean week2;
	private boolean week3;
	private boolean week4;
	private boolean cap_pref;
	public Player(String id, String name, String gender, long phone, boolean week1, boolean week2, boolean week3,
			boolean week4, boolean cap_pref) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.phone = phone;
		this.week1 = week1;
		this.week2 = week2;
		this.week3 = week3;
		this.week4 = week4;
		this.cap_pref = cap_pref;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public long getPhone() {
		return phone;
	}
	public void setPhone(long phone) {
		this.phone = phone;
	}
	public boolean isWeek1() {
		return week1;
	}
	public void setWeek1(boolean week1) {
		this.week1 = week1;
	}
	public boolean isWeek2() {
		return week2;
	}
	public void setWeek2(boolean week2) {
		this.week2 = week2;
	}
	public boolean isWeek3() {
		return week3;
	}
	public void setWeek3(boolean week3) {
		this.week3 = week3;
	}
	public boolean isWeek4() {
		return week4;
	}
	public void setWeek4(boolean week4) {
		this.week4 = week4;
	}
	public boolean isCap_pref() {
		return cap_pref;
	}
	public void setCap_pref(boolean cap_pref) {
		this.cap_pref = cap_pref;
	}
	
}
