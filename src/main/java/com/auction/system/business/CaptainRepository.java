package com.auction.system.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CaptainRepository {
	private CaptainData dao;

	@Autowired
	public CaptainRepository(CaptainData captain) {
		this.dao = captain;
	}
	
	public List<Captain> allCaptains(){
		return dao.allCaptains();
	}
	
	public Captain getCaptainById(String id) {
		return dao.getCaptainById(id);
	}
	
	public Captain validateCaptain(String id, String password) {
		return dao.validateCaptain(id, password);
	}
	
	public int makeCaptains(List<Captain> captains) {
		return dao.makeCaptains(captains);
	}

	public List<Captain> getNominatedCaptains() {
		return dao.getNominatedCaptains();
	}
	public int checkCaptains(List<Captain> captains) {
		return dao.checkCaptains(captains);
	}
	public int uncheckCaptains(List<Captain> captains) {
		return dao.uncheckCaptains(captains);
	}

	public int putRatings(String p_id, String c_id,int rating) {
		return dao.putRatings(p_id, c_id, rating);
	}
}
