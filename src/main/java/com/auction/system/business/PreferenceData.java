package com.auction.system.business;

import java.util.List;

public interface PreferenceData {
	public List<Preference> getSortedPlayersByPref();
	public List<Player> getRoundAPlayers();
	public List<Preference>getPreference(); 
	public List<Preference>getPreferenceByCaptainId(String captainId);
}