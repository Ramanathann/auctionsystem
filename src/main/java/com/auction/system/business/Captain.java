package com.auction.system.business;

public class Captain {
	private String id;
	private int credits;
	private int status;
	private String password;
	private String name;
	public Captain(String id, int credits, int status, String password, String name) {
		super();
		this.id = id;
		this.credits = credits;
		this.status = status;
		this.password = password;
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getCredits() {
		return credits;
	}
	public void setCredits(int credits) {
		this.credits = credits;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
