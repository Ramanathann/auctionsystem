package com.auction.system.business;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuctionRepository {
	
	private AuctionData dao;
	
	@Autowired
	public AuctionRepository(AuctionData auctionData) {
		this.dao = auctionData;
	}
	
	public List<Auction> getPlayersByCaptain(String soeid){
		return this.dao.getPlayersByCaptain(soeid);
	}

	public int getSoldPriceById(String soeid) {
		return this.dao.getSoldPriceById(soeid);
	}

	public int getNoOfPlayersByCaptain(String soeid) {
		return this.dao.getNoOfPlayersByCaptain(soeid);
	}

	public Map<String, List<Player> > getPlayersAndCaptain(List<Captain> captains){
		return this.dao.getPlayersAndCaptain(captains);
	}
	
	public Map<String, Integer> getNoOfPlayersByAllCaptains(List<Captain> captains){
		return this.dao.getNoOfPlayersByAllCaptains(captains);
	}
	
	public int addAuctionRecord(Auction auction) {
		return this.dao.addAuctionRecord(auction);
	}
	
	public List<TeamStatus> getAllTeamStatus(){
		return this.dao.getAllTeamStatus();
	}
	public List<Player> getRoundBPlayers(){
		return this.dao.getRoundBPlayers();
	}

}
