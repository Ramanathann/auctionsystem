package com.auction.system.business;

import java.util.List;
import java.util.Map;

public interface AuctionData {
	public List<Auction> getPlayersByCaptain(String soeid);

	public int getSoldPriceById(String soeid);

	public int getNoOfPlayersByCaptain(String soeid);

	public Map<String, List<Player>> getPlayersAndCaptain(List<Captain> captain_ids);

	public Map<String, Integer> getNoOfPlayersByAllCaptains(List<Captain> captain_ids);

	public int addAuctionRecord(Auction auction);
	
	public List<TeamStatus> getAllTeamStatus();
	
	public List<Player> getRoundBPlayers();

}