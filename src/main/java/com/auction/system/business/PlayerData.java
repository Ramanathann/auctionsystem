package com.auction.system.business;

import java.util.List;


public interface PlayerData {
	public List<Player> allPlayers();
	public List<Player> onlyPlayers();
	public List<Player> getCaptains();
	public Player getPlayerById(String id);
	public int deletePlayerById(String id);
	public Player addPlayer(Player player);
	public int addCaptain(String id);
	public int addPreference(int value);
	public List<Player> playersByPage(int pageid);
}

