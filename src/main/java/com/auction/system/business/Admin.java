package com.auction.system.business;

public class Admin {
	private String soeid;
	private String password;
	public Admin(String soeid, String password, int role) {
		super();
		this.soeid = soeid;
		this.password = password;
		this.role = role;
	}
	public String getSoeid() {
		return soeid;
	}
	public void setSoeid(String soeid) {
		this.soeid = soeid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getRole() {
		return role;
	}
	public void setRole(int role) {
		this.role = role;
	}
	private int role;
	
	
}
