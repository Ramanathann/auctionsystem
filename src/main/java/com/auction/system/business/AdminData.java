package com.auction.system.business;

import java.util.List;

public interface AdminData {
	public List<Admin> getAllAdmins();
	public Admin getAdminById(String soeid);
	public Admin getAdminByIdAndPassword(String soeid, String password);
}