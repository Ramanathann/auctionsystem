$(document).ready(function(){
    populateTable();
});

function populateTable(){
	$("#registeredCaptainsfinal tbody").html("");
	$.ajax({
        url: '/getNomCaptains',
        type: 'get',
        dataType: 'JSON',
        success: function(response){
        	$("#registeredCaptainsfinal tbody").html("");
            let len = response.length;
            for(let i=0; i<len; i++){
                let id = response[i].id;
               let name = response[i].name;
                let credits = response[i].credits;
                let status = (response[i].status == true) ? "&#10004;" : "&#10006;";
                
                let tr_str = "<tr>" +
                    "<td align='center' id=" + id + " name=" + id +">" + id + "</td>" +
                    "<td align='center' name=" + name + ">" + name + "</td>" +
                    
                    "</tr> ";

                $("#registeredCaptainsfinal tbody").append(tr_str);
            }
        }
    });
}