$(document).ready(function(){
    populateTable();
});

function populateTable(){
	$("#finalCaptains tbody").html("");
	$.ajax({
        url: '/viewRatings',
        type: 'get',
        dataType: 'JSON',
        success: function(response){
        	$("#ratings_view tbody").html("");
            let len = response.length;
            for(let i=0; i<len; i++){
                let id = response[i].player_id;
                let name = response[i].name;
                let preference= response[i].rating;
                
                let tr_str = "<tr>" +
                    "<td align='center' id=" + id + " name=" + id +">" + id + "</td>" +
                    "<td align='center' name=" + name + ">" + name + "</td>" +
                    "<td align='center' preference=" + preference + ">" + preference + "</td>" +
                   
                    "</tr> ";

                $("#ratings_view tbody").append(tr_str);
            }
        }
    });
}


