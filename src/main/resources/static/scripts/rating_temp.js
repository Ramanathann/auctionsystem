var totalRecords = 0;
$(document).ready(function(){
    console.log("Populate over");
    var tbl = $.parseJSON($.ajax({
        url: "/allplayers",
        type:"get",
        async: false,
        dataType: "json"
    }).responseText);
    totalRecords = tbl.length;
    var table = $("#ratings").DataTable({
    	data:tbl,
        columns: [
            { "data": "id" },
            { "data": "name"}
        ],
        "aoColumnDefs": [            
            {
                "aTargets": [ 2 ],
                "mRender": function (data,type,row) {
                	if(type === 'display'){
                    	console.log(row.id);
                    	return popsDropDown(row);
                	}
                }
            }
        ]
    });
    $(document).on('change','select',function(e){
//    	$('option').prop('disabled', false);
    	$(this).find("option:selected").each(function(){
    		var val = $(this).val();
    		if(val != 0){
			$('select').not(this).find('option').filter(function() {
				return $(this).val() === val;
			}).prop('disabled',true);
    		}
    	})
	});
    console.log("Datatable over");
});

function disableRatings(){
	$('select').on('change', function() {
		$('option').prop('disabled', false);
		$('select').each(function() {
			var val = this.value;
			if(val != 0){
			    $('select').not(this).find('option').filter(function() {
			    	return this.value === val;
				    }).prop('disabled', true);
			}
		  });
	}).change(); 
}

var ratingList = [
	{"value": 0, "selected": false},
	{"value": 1, "selected": false},
	{"value": 2, "selected": false},
	{"value": 3, "selected": false},
	{"value": 4, "selected": false},
	{"value": 5, "selected": false},
	{"value": 6, "selected": false},
	{"value": 7, "selected": false},
	{"value": 8, "selected": false},
	{"value": 9, "selected": false},
	{"value": 10, "selected": false},
	{"value": 11, "selected": false},
	{"value": 12, "selected": false},
	{"value": 13, "selected": false},
	{"value": 14, "selected": false},
	{"value": 15, "selected": false}
];

function popsDropDown(row) {
//	var option_str = "<select id='" + row.id + "' name='" + row.id + "'>";
	var option_str = "<select id='drp-down'>";
	for(var j = 0; j < ratingList.length; j++){
		if (ratingList[j].selected == false){
			option_str = option_str + "<option value=" + ratingList[j].value + ">" +
			ratingList[j].value + "</option>";
		}
	}
	option_str = option_str + "</select>";
	return option_str;
}


function validateFifteenPlayers() {
	let n = $(this).find('input[name="captains"]:checked').length;
	console.log("Total Selected " + n);
	if( n == 6){
		$("captainError").text("");
		return true;
	}
	else{
		console.log("Captain Count less");
		alert("Select 6 captains");
		$("captainerror").text("Select 6 Captains");
		return false;
	}
	return true;
}