$(document).ready(function(){
    populateTable();
    $(document).on('change','select',function(e){
    	$('option').prop('disabled', false);
    	$(this).find("option:selected").each(function(){
    		var val = $(this).val();
    		if(val != 0){
			$('select').not(this).find('option').filter(function() {
				return $(this).val() === val;
			}).prop('disabled',true);
    		}
    	})
	});
    console.log("Datatable over");
});
var $pagination = $("#pagination-demo"),
totalRecords = 0,
records = [],
displayRecords = [],
recPerPage = 10,
page = 1,
Pages = 0;

var ratingList = [
	{"value": 0, "selected": false},
	{"value": 1, "selected": false},
	{"value": 2, "selected": false},
	{"value": 3, "selected": false},
	{"value": 4, "selected": false},
	{"value": 5, "selected": false},
	{"value": 6, "selected": false},
	{"value": 7, "selected": false},
	{"value": 8, "selected": false},
	{"value": 9, "selected": false},
	{"value": 10, "selected": false},
	{"value": 11, "selected": false},
	{"value": 12, "selected": false},
	{"value": 13, "selected": false},
	{"value": 14, "selected": false},
	{"value": 15, "selected": false}
];

var playerRating = [];

function populateTable(){
	$("#playersBody").html("");
	$.ajax({
        url: '/allplayers',
        type: 'get',
        dataType: 'JSON',
        success: function(data){
            let len = data.length;
            records = data;
            console.log(records);
            totalRecords = records.length;
            Pages = Math.ceil(totalRecords/recPerPage);
            console.log("Total Pages "+Pages);
            apply_pagination();
        }
    });
}

function apply_pagination() {
	console.log("Inside apply_pagination");
	console.log("Pages "+Pages);
	$("#pagination-demo").twbsPagination({
		totalPages: Pages,
        visiblePages: 6,
        onPageClick: function (event, page) {
        	displayRecordsIndex = Math.max(page - 1, 0) * recPerPage;
        	endRec = (displayRecordsIndex) + recPerPage;
        	displayRecords = records.slice(displayRecordsIndex, endRec);
        	generatetable();
        	}
	});
}

function generatetable() {
	$("#playersBody").html("");
	for(var i = 0; i < displayRecords.length; i++){
		console.log("displayRecords length is " + displayRecords.length);
		console.log("ID" + displayRecords[i].id);
		console.log("Name" + displayRecords[i].name);
		console.log("Phone" + displayRecords[i].phone);
		let tr_str = "<tr>" +
		"<td align='center'>" + displayRecords[i].id + "</td>" +
		"<td align='center'>" + displayRecords[i].name + "</td>" +
		"<td align='center'>" + 
		"<select id=" + displayRecords[i].id + " name=" + displayRecords[i].id + ">" +
		"</select>" +
		"</td>" + "</tr>";
		$("#playersBody").append(tr_str);
		populateDropdown();
	}
}

function populateDropdown() {
	for(var i = 0; i < displayRecords.length; i++){
		$("#"+displayRecords[i].id).html("");
		let option_str = "";
		for(var j = 0; j < ratingList.length; j++){
			if (ratingList[j].selected == false){
				option_str = option_str + "<option value=" + ratingList[j].value + ">" + 
				ratingList[j].value + "</option>";
			}
		}
		$("#"+displayRecords[i].id).append(option_str);
    }
}

function disableRatings(){
	$('select').on('change', function() {
		$('option').prop('disabled', false);
		$('select').each(function() { 
			var val = this.value;
			if(val != 0){
			    $('select').not(this).find('option').filter(function() {
			    	return this.value === val;
				    }).prop('disabled', true);
			}
		  });
	}).change(); 
}

function validateFifteenPlayers() {
	let n = $(this).find('input[name="captains"]:checked').length;
	console.log("Total Selected " + n);
	if( n == 6){
		$("captainError").text("");
		return true;
	}
	else{
		console.log("Captain Count less");
		alert("Select 6 captains");
		$("captainerror").text("Select 6 Captains");
		return false;
	}
	return true;
}