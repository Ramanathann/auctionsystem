/*Loading register.html into div*/
$(document).ready(function(){
   $('#admincontent').load("admin.html");
});

$(document).ready(function(){
	$("#adminLogin").click(function(){
		$("#soeidAdminError").text("");
		$("#passwordAdminError").text("");
		$("#exampleModal1").modal();
	});
});
function validateAdmin() {
	let soeidcheck=validateAdminSOEID();
	let passwordcheck=validateAdminPassword();
	console.log("In validate admin");
	console.log(soeidcheck+" "+passwordcheck);
	if(soeidcheck && passwordcheck)
		{
		return true;
		}
	else
		return false;
}

function validateAdminSOEID() {
	let soeidPattern = /^[a-zA-z]{2}[\d]{5}$/;
	let soeid = $("#soeidAdmin").val();
	if(!soeidPattern.test(soeid)) {
		$("#soeidAdminError").text("SOEID format is incorrect!");
		return false;
	}
	else {
		$("#soeidAdminError").text("");
		return true;
	}
}

function validateAdminPassword() {
	//let namePattern = /^[a-zA-z]+$/;
	let password = $("#passwordAdmin").val();
	if(password==""||password==null||password.length<8) {
		$("#passwordAdminError").text("Password should be minimum of 8 characters.");
		return false;
	}
	else {
		$("#passwordAdminError").text("");
		return true;
	}
}
