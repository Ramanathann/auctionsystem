$(document).ready(function(){
    populateTable();
});

function populateTable(){
	$("#finalCaptains tbody").html("");
	$.ajax({
        url: '/allCaptains',
        type: 'get',
        dataType: 'JSON',
        success: function(response){
        	$("#finalCaptains tbody").html("");
            let len = response.length;
            for(let i=0; i<len; i++){
                let id = response[i].id;
               let name = response[i].name;
                let credits = response[i].credits;
                let status = (response[i].status == true) ? "&#10004;" : "&#10006;";
                
                let tr_str = "<tr>" +
                    "<td align='center' id=" + id + " name=" + id +">" + id + "</td>" +
                    "<td align='center' name=" + name + ">" + name + "</td>" +
                   
                    "</tr> ";

                $("#finalCaptains tbody").append(tr_str);
            }
        }
    });
}


