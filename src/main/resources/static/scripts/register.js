/*Loading register.html into div*/
$(document).ready(function(){
   $('#content').load("register.html");
});

$(document).ready(function(){
	$("#registerLink").click(function(){
		$("#soeidError").text("");
		$("#nameError").text("");
		$("#contactError").text("");
		$("#weekError").text("");
		$("#exampleModal").modal();
	});
});

function validate() {
	let soeidCheck = validateID();
	let nameCheck = validateName();
	let contactCheck = validateContact();
	let weekCheck = validateWeek();
	
	if(soeidCheck && nameCheck && contactCheck && weekCheck) {
		return true;
	}
	else {
		return false;
	}
}

function validateID() {
	let soeidPattern = /^[a-zA-z]{2}[\d]{5}$/;
	let soeid = $("#soeid").val();
	if(!soeidPattern.test(soeid)) {
		$("#soeidError").text("SOEID format is incorrect!");
		return false;
	}
	else {
		$("#soeidError").text("");
		return true;
	}
}

function validateName() {
	let namePattern = /^[a-zA-Z][a-zA-Z .']*$/;
	let name = $("#name").val();
	if(!namePattern.test(name)) {
		$("#nameError").text("Name format is incorrect!");
		return false;
	}
	else {
		$("#nameError").text("");
		return true;
	}
}

function validateContact() {
	let contactPattern = /^[\d]{10}$/;
	let contact = $("#contact").val();
	if(!contactPattern.test(contact)) {
		$("#contactError").text("Contact format is incorrect!");
		return false;
	}
	else {
		$("#contactError").text("");
		return true;
	}
}

function validateWeek() {
	if(!$("#weekOne").is(':checked') && !$("#weekTwo").is(':checked') 
			&& !$("#weekThree").is(':checked') && !$("#weekFour").is(':checked')) {
		$("#weekError").text("Atleast one week must be selected");
		return false;
	} 
	else {
		$("#weekError").text("");
		return true;
	}
}