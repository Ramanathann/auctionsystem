/*Loading register.html into div*/
$(document).ready(function(){
   $('#captaincontent').load("captain.html");
});
$(document).ready(function(){
	$("#captainLogin").click(function(){
		$("#soeidCaptainError").text("");
		$("#passwordCaptainError").text("");
		$("#exampleModal2").modal();
	});
});

function validateCaptain() {
	let csoeid=validateCaptainSOEID();
	console.log("In side captaijn validate");
	let cpassword=validateCaptainPassword();
	if(csoeid && cpassword)
		{
		return true;
		}
	else
		{
		return false;
		}
}

function validateCaptainSOEID() {
	let soeidPattern = /^[a-zA-z]{2}[\d]{5}$/;
	let soeid = $("#soeidCaptain").val();
	if(!soeidPattern.test(soeid)) {
		$("#soeidCaptainError").text("SOEID format is incorrect!");
		return false;
	}
	else {
		$("#soeidCaptainError").text("");
		return true;
	}
}

function validateCaptainPassword() {
	//let namePattern = /^[a-zA-z]+$/;
	let password = $("#passwordCaptain").val();
	if(password==""||password==null||password.length<5) {
		$("#passwordCaptainError").text("Password should be minimum of 5 characters.");
		return false;
	}
	else {
		$("#passwordCaptainError").text("");
		return true;
	}
}
