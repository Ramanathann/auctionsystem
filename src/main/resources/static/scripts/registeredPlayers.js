$(document).ready(function(){
    populateTable();
});

function populateTable(){
	$("#registeredPlayers tbody").html("");
	$.ajax({
        url: '/allplayers',
        type: 'get',
        dataType: 'JSON',
        success: function(response){
            let len = response.length;
            for(let i=0; i<len; i++){
                let id = response[i].id;
                let name = response[i].name;
                let phone = response[i].phone;
                let gender = (response[i].gender == "M") ? "Male" : "Female";
                let week1 = (response[i].week1 == true) ? "&#10004;" : "&#10006;";
                let week2 = (response[i].week2 == true) ? "&#10004;" : "&#10006;";
                let week3 = (response[i].week3 == true) ? "&#10004;" : "&#10006;";
                let week4 = (response[i].week4 == true) ? "&#10004;" : "&#10006;";
                let cap_pref = (response[i].cap_pref == true) ? "&#10004;" : "&#10006;";
                
                let tr_str = "<tr>" +
                    "<td align='center' >" + id + "</td>" + 
                    "<td align='center'>" + name + "</td>" +
                    "<td align='center'>" + phone + "</td>" +
                    "<td align='center'>" + gender + "</td>" +
                    "<td align='center'>" + week1 + "</td>" +
                    "<td align='center'>" + week2 + "</td>" +
                    "<td align='center'>" + week3 + "</td>" +
                    "<td align='center'>" + week4 + "</td>" +
                    "<td align='center'>" + cap_pref + "</td>" +
                    "</tr>";
                $("#registeredPlayers tbody").append(tr_str);
            }
            splitTable();
        }
    });
}

function splitTable() {
    let rowsShown = 10;
    let rowsTotal = $('#tableBody tr').length;
    console.log("Hi"+rowsTotal);
    let numPages = rowsTotal/rowsShown;
    
    let pageNum = 1;
    $('#nav').append('<li class="page-item"><a class="page-link" href="#" rel="'+1+'">'+pageNum+'</a></li>');
    for(i = 1;i < numPages;i++) {
        let pageNum = i + 1;
        $('#nav').append('<li class="page-item"><a class="page-link" href="#" rel="'+i+'">'+pageNum+'</a></li>');
        //$('#nav').append('<button href="#" class="btn btn-outline-primary" style="padding-left:25px;padding-right:25px;margin-right:10px" '+
        //		'rel="'+i+'">'+pageNum+'</button>');
    }
    $('#tableBody tr').hide();
    $('#tableBody tr').slice(0, rowsShown).show();
    $('#nav a:first').addClass('active');
    $('#nav a').bind('click', function(){

        $('#nav a').removeClass('active');
        $(this).addClass('active');
        let currPage = $(this).attr('rel');
        let startItem = currPage * rowsShown;
        let endItem = startItem + rowsShown;
        $('#tableBody tr').css('opacity','0.0').hide().slice(startItem, endItem).
        css('display','table-row').animate({opacity:1}, 300);
    });
}