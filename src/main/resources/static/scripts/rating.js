$(document).ready(function(){
    populateTable();
    $(document).on('change','select',function(e){
    	$(this).find("option:selected").each(function(){
    		var val = $(this).val();
    		if(val != 0){
			$('select').not(this).find('option').filter(function() {
				return $(this).val() === val;
			}).prop('disabled',true);
    		}
    	})
	});
});
var $pagination = $("#pagination-demo"),
totalRecords = 0,
records = [],
displayRecords = [],
recPerPage = 10,
page = 1,
Pages = 0;
var ratingList = [
	{"value": 0, "selected": false},
	{"value": 1, "selected": false},
	{"value": 2, "selected": false},
	{"value": 3, "selected": false},
	{"value": 4, "selected": false},
	{"value": 5, "selected": false},
	{"value": 6, "selected": false},
	{"value": 7, "selected": false},
	{"value": 8, "selected": false},
	{"value": 9, "selected": false},
	{"value": 10, "selected": false},
	{"value": 11, "selected": false},
	{"value": 12, "selected": false},
	{"value": 13, "selected": false},
	{"value": 14, "selected": false}
];

var playerRating = [];

function populateTable(){
	$("#playersBody").html("");
	$.ajax({
        url: '/onlyPlayers',
        type: 'get',
        dataType: 'JSON',
        success: function(data){
            let len = data.length;
            records = data;
            console.log(records);
            totalRecords = records.length;
            Pages = Math.ceil(totalRecords/recPerPage);
            console.log("Total Pages "+Pages);
            generatetables();
        }
    });
}

function generatetables() {
	$("#playersBody").html("");
	for(var i = 0; i < totalRecords; i++){
		let tr_str = "<tr>" +
		"<td align='center'>" + records[i].id + "</td>" +
		"<td align='center'>" + records[i].name + "</td>" +
		"<td align='center'>" + 
		"<select id=" + records[i].id + " name=" + records[i].id + ">" +
		"</select>" +
		"</td>" + "</tr>";
		$("#playersBody").append(tr_str);
		populateDropdown();
	}
}

function populateDropdown() {
	for(var i = 0; i < records.length; i++){
		$("#"+records[i].id).html("");
		let option_str = "";
		for(var j = 0; j < ratingList.length; j++){
			if (ratingList[j].selected == false){
				option_str = option_str + "<option value=" + ratingList[j].value + ">" + 
				ratingList[j].value + "</option>";
			}
		}
		$("#"+records[i].id).append(option_str);
    }
}

function displaySelected(){
	var ratingData = [];
	var datas = {};
	var $ratings = [];
	var rating = function(id, value){
		this.player_id = id;
		this.ratings = value;
	}
	for(var i =0; i < records.length; i++){
		value = $("#" + records[i].id + " :selected").val();
		if(value != 0) {
			$ratings.push(new rating(records[i].id,value));
		}
	}
	datus = JSON.stringify({'datas': datas});
	if($ratings.length !== 14){
		alert("Select 14 Players");
		return false;
	}
	else{
		$.ajax({
			type:"POST",
			contentType:"application/json; charset=utf-8",
			url:"/putratings",
			data: JSON.stringify($ratings),
	        dataType: 'json',
	        cache: false,
	        timeout: 600000,
	        success: function(data){
	        	location.href = "./viewRating";
	        	console.log("SUCCESS "+data);
	        },
	        error: function(e){
	        	console.log("ERROR "+e);
	        }
		});
		return true;
	}
}

function resetValues() {
	$('select option[value="0"]').prop('selected', true);
	$('option').prop('disabled', false);
}