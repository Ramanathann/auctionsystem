$(document).ready(function(){
    populateTable();
});

function populateTable(){
	$("#nominatedCaptains tbody").html("");
	$.ajax({
        url: '/getNomCaptains',
        type: 'get',
        dataType: 'JSON',
        success: function(response){
            let len = response.length;
            for(let i=0; i<len; i++){
                let id = response[i].id;
                let name = response[i].name
                let credits = response[i].credits;
                let status = (response[i].status == true) ? "&#10004;" : "&#10006;";

                let tr_str = "<tr>" +
                    "<td align='center' id=" + id + " name='soeids'>" + id + "</td>" +
                    
                    "<td align='center' style='display:none'><input type='hidden' name='soeid' value=" + id +  " /></td>" +
                    "<td align='center' name=" + name + ">" + name + "</td>" +
                    "<td align='center' style='display:none'><input type='hidden' name='name' value=" + name +  " /></td>" +
                    "</tr>";
                $("#nominatedCaptains tbody").append(tr_str);
            }
        }
    });
}