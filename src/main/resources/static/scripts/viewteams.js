var maxPlayerCount;
$(document).ready(function(){
	maxPlayerCount = 1;
	populateTeamCards();
});

function captainStatus(){
	$.ajax({
		url: '/getNomCaptains',
		type: 'get',
		dataType: 'JSON',
		success: function(captains){
			let str = "";
			if(captains.length == 0){
				console.log("Hello world", maxPlayerCount);
				if(maxPlayerCount > 1){
					str = "<p> The captains have been nominated by the organizers and the bidding is over.  Below are the list of captains and their teams!!";
				}
				else {
					str = "<p> Please wait for the nomination of captains by the makers!!"
				}
			} else {
				str = "<p> The captains are nominated by the makers but not yet approved by the checkers!!";
			}
			$("#infoDiv").html("");
			$("#infoDiv").append(str);
		}
	});
}
function populateTeamCards(){
	$.ajax({
		url: '/viewteams',
		type: 'get',
		dataType: 'JSON',
		success: function(teams){
			let index = 1;
			for(let captain_id in teams){
				let players = teams[captain_id];
				maxPlayerCount = players.length > maxPlayerCount ? players.length : maxPlayerCount;
				console.log(players.length, maxPlayerCount);
				let team = "<div class='col-lg-4 col-sm-6 portfolio-item p-5'>"+
				"<div class='card' style='width: 18rem;'>"+
					"<div class='card-header'>Team " + index + "</div>" + 
					"<ul class='list-group list-group-flush'>" + 
					"<li class='list-group-item'>"+ players[0].name + " (" + players[0].id + ") " + "<span class='badge badge-info'>captain</span></li>";
				for (let i = 1; i < players.length; i++){
					team += "<li class='list-group-item'>"+ players[i].name + " (" + players[i].id + ")" + "</li>";
				}
				team += "</ul> </div> </div>";
				$("#teams").append(team);
				index++;
			}
			captainStatus();
		}
	});
}