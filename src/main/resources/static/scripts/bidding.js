var curr_bid = null;
var teamsInfo = null;
var allcaptains;
var max_wallet = 50;
$(document).ready(function(){
	$("#errorDiv").hide();
	populateCaptainId();
	updateTeamInfo();
	getNextBid();
});

function doEnd(){
	return confirm("Are you sure you want to end round A auction?");
}

function populateCaptainId(){
	$.ajax({
		url: '/allCaptains',
		type: 'get',
		dataType: 'JSON',
		success: function(captains){
			allcaptains = captains;
			$("#captainsList").html("");
			for(let i = 0; i < captains.length; i++){
				let str = "<option value=" + captains[i].id + ">";
				$("#captainsList").append(str);
			}
		}
	});
}

function updateTeamInfo(){
	$.ajax({
        url: '/updateteaminfo',
        type: 'get',
        dataType: 'JSON',
        success: function(teams){
        	teamsInfo = teams;
        	$("#captainCards").html("");
        	let index = 1;
        	for(let i = 0; i < teams.length; i++){
            	let card = "<div class='card bg-light m-3' style='max-width: 18rem;'>"+
				"<div class='card-header'><h5>Captain " + index + "</h5></div>"+
				"<div class='card-body'>"+
					"<table border=0 class='card-table table'>"+
						"<tbody>"+
							"<tr>"+
								"<td><span class='badge badge-pill badge-primary'>Captain"+
										"ID</span></td>"+
								"<td>" + teams[i].captainId + "</td>"+
							"</tr>"+
							"<tr>"+
								"<td><span class='badge badge-pill badge-primary'>Players "+
										"Bought</span></td>"+
								"<td>"+teams[i].playersBought+"</td>"+
							"</tr>"+
							"<tr>"+
								"<td><span class='badge badge-pill badge-primary'>Money Available"+
										"</span></td>"+
								"<td>"+(max_wallet - teams[i].moneySpent)+"</td>"+
							"</tr>"+
						"</tbody>"+
					"</table>"+
				"</div>"+
			"</div>"
        	$("#captainCards").append(card);
			index++;
        	}
        }
	});
}

function getNextBid(){
	$.ajax({
		url: '/nextbid',
		type: 'get',
		dataType: 'JSON',
		success: function(player){
			if (curr_bid == null && player == null){
				$("#playerInfo").html("");
				let str = "<div class='alert alert-info' role='alert'>" +
							"<h4 class='alert-heading'>Captain Preference Not Updated</h4>" +
							"<p>The captains have not updated their player preference yet.  Without updating, the auction cannot be started</p>" +
							"</div>";
				$("#playerInfo").append(str);

			}
			else if (player == null){
				$("#playerInfo").html("");
				let str = "<div class='alert alert-info' role='alert'>" +
							"<h4 class='alert-heading'>Auction Round 1 - Over</h4>" +
							"<p>Aww yeah, all the players in the Round 1 have been sold out to the captains.  No players left.</p>" +
							"<hr>" +
							"<p class='mb-0'>Please End Bid to automatically conduct Round 2 of Auction.</p>" +
							"</div>";
				$("#playerInfo").append(str);
			}
			else{
				curr_bid = player;
				$("#playerInfo").html("");
				$("#captainId").val("");
				$("#soldPrice").val("");
				let availDict = {0: "Not Available", 1: "Available"};
				let availClassDict = {0: "badge badge-warning", 1: "badge badge-success"};
				let availabilityFields = ['week1', 'week2', 'week3', 'week4'];
				let playerinfo = "<table border=0 class='card-table table table-striped'>" +
									"<thead class='thead-dark'>"+
										"<tr>"+
											"<th>Label</th>" +
											"<th>Player Info</th>" +
										"</tr>" +
									"</thead>" +
									"<tbody>";
				for(let key in player){
					if (key == "cap_pref"){
						continue;
					}
					playerinfo += "<tr>" +
										"<td>" + key + "</td>";
					if (availabilityFields.includes(key)){
						playerinfo += "<td><span class = '"+availClassDict[player[key]|0]+"'>" + availDict[player[key]|0] + "</td></tr>";
					}
					else {
						playerinfo += "<td>" + player[key] + "</td></tr>";
					}
				}
				playerinfo += "</tbody></table>"
				$("#playerInfo").append(playerinfo);
			}
			
		}
	});
}
function nextBid(){
	if (curr_bid != null){
		$("#errorDiv").html("");
		$("#errorDiv").hide();
		let bid = { "player_id":curr_bid.id,"captain_id":$("#captainId").val(),"sold_price":parseInt($("#soldPrice").val(),10) };
		console.log(bid);
		if (validateBid(bid)){
			if ("" == bid.captain_id || validateCaptain(bid.captain_id)){
				$.ajax({
					url: '/addbid',
					type: 'post',
					contentType: 'application/json',
					data: JSON.stringify(bid),
					success: function(status){
						if(status){
							getNextBid();
							updateTeamInfo();
						} else{
						}
					}
				});

			} else {
				let errormsg = "The captain_id <strong>" + bid.captain_id + "</strong> doesn't seem to be of a valid captain!!";
				$("#errorDiv").append(errormsg);
				$("#errorDiv").show();
			}
		} else{
			let errormsg = "The wallet amount of <strong>" + bid.captain_id + "</strong> is not sufficient to make the bid!!";
			$("#errorDiv").append(errormsg);
			$("#errorDiv").show();
		}
	}	
}

function validateBid(bid){
	if (bid.sold_price < 0 || bid.sold_price > 50){
		return false;
	}
	for(let i = 0; i < teamsInfo.length; i++){
		if (teamsInfo[i].captainId == bid.captain_id){
			if(teamsInfo[i].moneySpent + bid.sold_price <= max_wallet){
				return true;
			}
			else{
				return false;
			}
		}
	}
	return true;
}

function validateCaptain(captain_id){
	for(let i = 0; i < allcaptains.length; i++){
		if(allcaptains[i].id == captain_id){
			return true;
		}
	}
	return false;
}

function endBid(){
	if (doEnd()){
		location.href='/viewroundbplayers.html'
	}
}

function logout(){
	return $.ajax({
		url: '/logoutadmin',
		type: 'get',
		dataType: 'boolean',
		success: function(status){
			return status;
		} 
	});
}